from google.appengine.ext import ndb

class SXP_Person(ndb.Model):
	"""
	Models a ShareXP User.
	"""
	name = ndb.StringProperty(verbose_name="Name")
	location = ndb.StringProperty(verbose_name="Location")
	email = ndb.StringProperty(verbose_name="E-mail")
	password = ndb.StringProperty(verbose_name="Password")
	token = ndb.StringProperty(verbose_name="User Token") #Facebook's
	cookie = ndb.StringProperty(verbose_name="Cookie") #Login cookie
	fb_id = ndb.StringProperty(verbose_name="Facebook ID")
	#There can't be a list of events here. There can be no cross-referencing between two classes

	def toJson(self):
		result = {}
		result[u'name'] = self.name
		result[u'location'] = self.location
		result[u'email'] = self.email
		result[u'id'] = self.key.id() if self.key else None
		result[u'fb_id'] = self.fb_id
		q = SXP_Sale.query(SXP_Sale.owner == self.key)
		result[u'num_sales'] = q.count()
		sales = q.fetch()
		num_rep = 0
		for sale in sales:
			if sale.num_report > 0:
				num_rep += 1
		result[u'num_reports'] = num_rep
		result[u'num_reports_made'] = SXP_SaleReport.query(SXP_SaleReport.reporter == self.key).count()
		return result


class SXP_Event(ndb.Model):
	"""
	Models a ShareXP Event.
	"""
	name = ndb.StringProperty(verbose_name="Name", required=True)
	owner = ndb.StructuredProperty(SXP_Person, required=True, verbose_name="Owner", repeated=False)
	guests = ndb.StructuredProperty(SXP_Person, repeated=True, verbose_name="Guests") #list of Person instances
	confirmedGuests = ndb.StructuredProperty(SXP_Person, repeated=True, verbose_name="Confirmed Guests") #same
	blockedGuests = ndb.StructuredProperty(SXP_Person, repeated=True, verbose_name="Blocked Guests") #same2
	selfInvitedGuests= ndb.StructuredProperty(SXP_Person, repeated=True, verbose_name="Self Invited Guests") #same3
	date = ndb.DateTimeProperty(verbose_name="Date", required=True)
	secret = ndb.BooleanProperty(default=False, verbose_name="Secret")

	def toJson(self):
		# TODO: finish this function
		result = {}
		return result

class SXP_EventList(ndb.Model):
	"""
	Maps every ShareXP user to a list of events he is somehow participating.
	"""
	person = ndb.StructuredProperty(SXP_Person, required=True, verbose_name="Person", repeated=False)
	event = ndb.StructuredProperty(SXP_Event, repeated=False, verbose_name="Event")

class SXP_Notification(ndb.Model):
	"""
	Models a ShareXP Notification.
	"""
	destination = ndb.StructuredProperty(SXP_Person, required=True, verbose_name="Destination", repeated=False)
	origin = ndb.StructuredProperty(SXP_Person, required=False, verbose_name="Origin", repeated=False)
	event = ndb.StructuredProperty(SXP_Event, required=False, verbose_name="Event", repeated=False)
	message = ndb.StringProperty(verbose_name="Message")
	date = ndb.DateTimeProperty(verbose_name="Date", required=True)
	not_type = ndb.StringProperty(verbose_name="Type", choices=["Sales", "Travels", "Meals"])
	sale_id = ndb.KeyProperty(verbose_name="Sale ID")
	travel_id = ndb.KeyProperty(verbose_name="Travel ID")
	meal_id = ndb.KeyProperty(verbose_name="Meal ID")

	def toJson(self, baseUrl):
		result = {}
		result[u'destination'] = self.destination.toJson()
		result[u'origin'] = self.origin.toJson() if self.origin else None
		result[u'event'] = self.event.toJson() if self.event else None
		result[u'message'] = self.message
		result[u'date'] = self.date
		result[u'not_type'] = self.not_type
		result[u'sale_id'] = self.sale_id.id() if self.sale_id else None
		result[u'travel_id'] = self.travel_id.id() if self.travel_id else None
		result[u'meal_id'] = self.meal_id.id() if self.meal_id else None
		result[u'id'] = self.key.id() if self.key else None
		return result

class SXP_Trigger(ndb.Model):
	"""
	Models a ShareXP trigger. These are alarms defined by users.
	"""
	trig_type = ndb.StringProperty(verbose_name="Type", choices=["Sales", "Travels", "Meals"])

class SXP_Merchant(ndb.Model):
	"""
	Models a ShareXP Merchant entity.
	"""
	name = ndb.StringProperty(verbose_name="Name", required=True)
	location = ndb.StringProperty(verbose_name="Location", required=True)
	geoPos = ndb.GeoPtProperty(verbose_name="Geographical Location", required=True)
	phone_number = ndb.StringProperty(verbose_name="Phone Number", required=False)
	country = ndb.StringProperty(verbose_name="Country", required=False)
	postal_code = ndb.StringProperty(verbose_name="Postal Code", required=False)
	city = ndb.StringProperty(verbose_name="City", required=False)
	county = ndb.StringProperty(verbose_name="County", required=False)
	state = ndb.StringProperty(verbose_name="State", required=False)

	def toJson(self, baseUrl):
		result = {u'name': unicode(self.name), 
			u'location': unicode(self.location),
			u'geo_location': {} }
		result[u'id'] = self.key.id() if self.key else None
		result[u'phone_number'] = unicode(self.phone_number)
		result[u'postal_code'] = unicode(self.postal_code)
		result[u'city'] = unicode(self.city)
		result[u'county'] = unicode(self.county)
		result[u'country'] = unicode(self.country)
		result[u'state'] = unicode(self.state)
		result[u'geo_location'][u'lat'] = self.geoPos.lat
		result[u'geo_location'][u'lon'] = self.geoPos.lon
		return result

class SXP_Product(ndb.Model):
	"""
	Models a ShareXP Product entity.
	"""
	name = ndb.StringProperty(verbose_name="Name", required=True)
	image = ndb.BlobKeyProperty(verbose_name="Product Picture")

	def toJson(self, baseUrl):
		img_url = unicode(baseUrl) + u'/prod_pic/' + unicode(self.image) if self.image else None
		result = {}
		result[u'name'] = unicode(self.name)
		result[u'image_url'] = img_url
		result[u'id'] = self.key.id() if self.key else None
		return result

class SXP_MerchantProducts(ndb.Model):
	"""
	Models the List of Products available at a Merchant's store.
	"""
	merchant = ndb.StructuredProperty(SXP_Merchant, required=True, verbose_name="Merchant")
	products = ndb.StructuredProperty(SXP_Product, verbose_name="Product")

class SXP_Sale(ndb.Model):
	"""
	Models a ShareXP Sale. Each sale is a product at a discount.

	Discount is a string to comprehend things such as "Any two for 3 pounds".
	"""
	end_date = ndb.DateTimeProperty(verbose_name="Date", required=True)
	valid = ndb.BooleanProperty(verbose_name="Valid", default=True)
	discount = ndb.StringProperty(verbose_name="Discount", required=True)
	product = ndb.KeyProperty(kind=SXP_Product, required=True, verbose_name="Product", repeated=False)
	merchant = ndb.KeyProperty(kind=SXP_Merchant, required=True, verbose_name="Merchant", repeated=False)
	price = ndb.FloatProperty(verbose_name="Original Price")
	owner = ndb.KeyProperty(kind=SXP_Person, verbose_name="Sale Creator", required=True)
	num_report = ndb.IntegerProperty(verbose_name="Number of Reported for Invalid", default=0)
	num_like = ndb.IntegerProperty(verbose_name="Number of Likes", default=0)

	def toJson(self, baseUrl, distance=None):
		result = {}
		result[u'id'] = unicode(self.key.id())
		result[u'end_date'] = unicode(self.end_date)
		result[u'price'] = unicode(self.price)
		result[u'valid'] = self.valid
		result[u'discount'] = self.discount
		merchant = SXP_Merchant.get_by_id(self.merchant.id())
		product = SXP_Product.get_by_id(self.product.id())
		owner = SXP_Person.get_by_id(self.owner.id())
		result[u'product'] = product.toJson(baseUrl)
		result[u'merchant'] = merchant.toJson(baseUrl)
		result[u'owner'] = owner.toJson()
		result[u'num_reports'] = self.num_report
		result[u'num_likes'] = self.num_like
		if distance:
				result[u'distance'] = distance
		q = SXP_SalesHashTag.query(SXP_SalesHashTag.sale == self.key)
		hashtags = q.fetch()
		result[u'hashtags'] = []
		for hashtag in hashtags:
			result[u'hashtags'].append(hashtag.toJson())
		return result

class SXP_HashTag(ndb.Model):
	"""
	Implements a HashTag
	"""
	tag = ndb.StringProperty(verbose_name="Tag", required=True)

	def toJson(self):
		return self.tag

class SXP_SalesHashTag(ndb.Model):
	"""
	Implements a list of hash tags for the Sales
	"""
	sale = ndb.KeyProperty(verbose_name="Sale", required=True, kind=SXP_Sale)
	hashtag = ndb.KeyProperty(verbose_name="Hashtag", required=True, kind=SXP_HashTag)

	def toJson(self):
		"""
		"""
		tag = SXP_HashTag.get_by_id(self.hashtag.id())
		if tag:
			return tag.toJson()
		else:
			return None

class SXP_SalesTrigger(ndb.Model):
	"""
	Models a Sales trigger.
	"""
	owner = ndb.KeyProperty(verbose_name="Owner", required=True, kind=SXP_Person)
	product = ndb.KeyProperty(verbose_name="Product", required=True, kind=SXP_Product)
	max_dist = ndb.FloatProperty(verbose_name="Maximum Distance", required=True)

class SXP_SalesLike(ndb.Model):
	"""
	Models who liked which sale.
	"""
	person = ndb.KeyProperty(verbose_name="Person", kind=SXP_Person, required=True)
	sale = ndb.KeyProperty(verbose_name="Reported Sale", kind=SXP_Sale, required=True)
	date = ndb.DateTimeProperty(verbose_name="Date", auto_now_add=True)

class SXP_SaleReport(ndb.Model):
	"""
	Models a Sales Report
	"""
	reporter = ndb.KeyProperty(verbose_name="Reporter", kind=SXP_Person, required=True)
	sale = ndb.KeyProperty(verbose_name="Reported Sale", kind=SXP_Sale, required=True)
	date = ndb.DateTimeProperty(verbose_name="Date", auto_now_add=True)

	def toJson(self, baseUrl):
		"""
		"""
		result = {}
		reporter = SXP_Person.get_by_id(self.reporter.id())
		result[u'reporter'] = reporter.toJson()
		sale = SXP_Sale.get_by_id(self.sale.id())
		result[u'sale'] = sale.toJson(baseUrl)
		return result