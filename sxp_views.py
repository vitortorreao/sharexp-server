import os
import webapp2
import jinja2
import urllib2
import urllib
import json
import Cookie
import hashlib
import datetime
import calendar
import email
import bcrypt
import math
import logging
import traceback
import re

from webapp2_extras import sessions
from sxp_models import *
from levenshteinDistance import levenshtein
from google.appengine.ext import blobstore
from google.appengine.ext import ndb
from google.appengine.ext.webapp import blobstore_handlers

# We are using bcrypt to salt and hash passwords.
# The library is available at https://github.com/erlichmen/py-bcrypt

# Jinja2 configuration
# Jinja2 is the template library we are using
# for more info: http://jinja.pocoo.org/docs/
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class BaseHandler(webapp2.RequestHandler):
    """
    Implements basic functionality. i.e. Session and Cookies
    """

    def dispatch(self):
        self.session_store = sessions.get_store(request=self.request)
        # Get a session store for this request.
        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)
            return

    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        return self.session_store.get_session()

    def clear_cookie(self,name,path="/",domain=None):
    	"""
    	Deletes the cookie with the given name.

    	This is done by setting the expiration to the past.
    	"""
    	expires = datetime.datetime.utcnow() - datetime.timedelta(days=365)
    	self.set_cookie(name,value="", expires=expires)

    def set_cookie(self, name, value, expires=None):
    	"""
    	Sets a new cookie to the given name and value.
    	"""
    	name = name
    	value = value
    	new_cookie = Cookie.BaseCookie()
    	new_cookie[name] = value
    	if expires:
    		timestamp = calendar.timegm(expires.utctimetuple())
    		new_cookie[name]["expires"] = email.utils.formatdate(timestamp,localtime=False,usegmt=True)
    	else:
    		# Number of days the cookie will have before expiring is in the "days" parameter
    		expires = datetime.datetime.utcnow() + datetime.timedelta(days=90)
    		timestamp = calendar.timegm(expires.utctimetuple())
    		new_cookie[name]["expires"] = email.utils.formatdate(timestamp,localtime=False,usegmt=True)
    	for value in new_cookie.values():
    		self.response.headers.add_header('Set-Cookie', value.OutputString(None))

    def clear_all_cookies(self):
    	"""
    	"""
    	pass

    def login_sucess(self, json_response, person):
    	"""
    	Writes the proper response after the login was successful.
    	Also creates the necessary cookies.

    	This can be called by either login view or register view.
    	"""
    	num = 0
    	for c in person.name:
    		num += ord(c)
    	secret = str(datetime.datetime.utcnow() - datetime.timedelta(days=num))
    	person.cookie = hashlib.sha256(secret).hexdigest()
    	person.put()
    	self.set_cookie("id", person.cookie)
    	json_response[u'status'] = True
    	json_response[u'username'] = person.name
    	json_response[u'email'] = person.email
    	json_response[u'location'] = person.location
    	json_response[u'token'] = person.token
    	self.response.write(json.dumps(json_response, indent=4))
    	return

    def pDecode(self, param):
    	"""
    	Decodes URL parameter to unicode
    	"""
    	return urllib.unquote_plus(urllib2.unquote(param.encode('utf-8'))).decode('utf-8')

class Login(BaseHandler):
	"""
	All the login functionality is present in this class.
	Note that all requests are GET. This is so the mobile app
	doesn't have to make POST requests.

	In the context of this class, username and user's e-mail
	are the same thing.
	"""

	FB_GRAPH_BASE_URL = "https://graph.facebook.com/me?access_token="

	def get(self):
		try:
			person_id = self.request.cookies["id"]
			self.redirect('/')
			return
		except KeyError:
			pass
		if not (self.request.get('username') or self.request.get('access_token')):
			#login form
			self.response.headers['Content-Type'] = 'text/html'
			context = {}
			template = JINJA_ENVIRONMENT.get_template('login.html')
			self.response.write(template.render(context))
			return
		else:
			#login logic
			self.response.headers['Content-Type'] = 'application/json'
			json_response = {u'status': False}
			if self.request.get('access_token'):
				#Facebook Log In using facebook's graph api
				user_info = {}
				try:
					URL = urllib2.urlopen(Login.FB_GRAPH_BASE_URL+self.request.get('access_token'))
					user_info = json.loads(URL.read())
				except urllib2.HTTPError:
					#Login failed
					self.response.write(json.dumps(json_response, indent=4))
					return
				if not "id" in user_info.keys():
					self.response.write(json.dumps(json_response, indent=4))
					return
				#Login successful. Now we need to know if it the user's first time or not
				#Query
				q = SXP_Person.query(SXP_Person.fb_id == user_info['id'])
				person = q.fetch()
				print(person)
				if len(person) <= 0:
					#User logging in for the first time using facebook
					#Create a new user
					person = SXP_Person()
					person.fb_id = user_info['id']
					if not "email" in user_info.keys():
						person.email = ""
					else:
						person.email = user_info['email']
					if not "name" in user_info.keys():
						person.name = ""
					else:
						person.name = user_info['name']
					person.token = self.request.get('access_token')
					person.put()
				else:
					#Not loggin in for the first time using facebook
					person = person[0]
					person.token = self.request.get('access_token') #update facebook token
					person.put() #update user entity
				#Login successful either way
				self.login_sucess(json_response, person)
				return
			else:
				#ShareXP Log In
				username = self.request.get('username')
				password = self.request.get('password')
				q = SXP_Person.query(SXP_Person.email == username)
				person = q.fetch()
				if len(person) <= 0:
					#failed
					self.response.write(json.dumps(json_response, indent=4))
					return
				person = person[0]
				if(person.password == bcrypt.hashpw(password, person.password)):
					#successful
					self.login_sucess(json_response, person)
					return
				else:
					#failed
					self.response.write(json.dumps(json_response, indent=4))
					return

class Register(BaseHandler):
	"""
	Class to handle the registering of new users.

	It is important for the mobile (Android) client that
	the recently registered user be logged in automatically.
	"""
	def register_fail(self, reason):
		json_response = {}
		json_response[u'status'] = False
		json_response[u'reason'] = reason
		self.response.write(json.dumps(json_response, indent=4))
		return

	def get(self):
		try:
			person_id = self.request.cookies["id"]
			self.redirect('/')
			return
		except KeyError:
			pass #if there is no cookie, continue to display the page normally
		if not (self.request.get('first_name') or self.request.get('last_name') or self.request.get('email') or
			self.request.get('password') or self.request.get('password_conf') or self.request.get('location')):
			#display register form
			self.response.headers['Content-Type'] = 'text/html'
			context = {}
			template = JINJA_ENVIRONMENT.get_template('register.html')
			self.response.write(template.render(context))
			return
		else:
			#register new user
			self.response.headers['Content-Type'] = 'application/json'
			json_response = {}
			name = self.request.get('first_name')+" "+self.request.get('last_name')
			if self.request.get('password') != self.request.get('password_conf'):
				self.register_fail(u'Password mismatch')
				return
			if len(self.request.get('password')) < 4:
				self.register_fail(u'Password too short')
				return
			if self.request.get('email') == "" or ("@" not in self.request.get('email')):
				self.register_fail(u'Invalid email')
				return
			#Check if e-mail is already registered
			q = SXP_Person.query(SXP_Person.email == self.request.get('email'))
			users = q.fetch()
			if len(users) > 0:
				self.register_fail(u'Email already registered')
				return
			# Register new user
			# Hash password first
			password = bcrypt.hashpw(self.request.get('password'), bcrypt.gensalt())
			# Now, create the person object
			person = SXP_Person()
			person.name = name
			person.password = password
			person.email = self.request.get('email')
			person.location = self.request.get('location')
			# store the new user in the database
			person.put()

			# Log the new user in
			self.login_sucess(json_response, person)


class Logout(BaseHandler):
	def get(self):
		person_id = ""
		try:
			person_id = self.request.cookies["id"]
		except KeyError:
			# If there is no user logged in
			self.redirect('/login')
			return
		q = SXP_Person.query(SXP_Person.cookie == person_id)
		users = q.fetch()
		if len(users) <= 0:
			# If the cookie does not belong to any user
			self.clear_cookie("id")
			self.redirect('/login')
			return
		# Log out user
		self.clear_cookie("id") # clear the cookie in the client
		person = users[0]
		person.cookie = "" # clear the cookie in the database
		person.put() # commit database changes
		self.redirect('/')
		# The mobile client should be satisfied by receiving a 300 HTTP response
		# The web user should be redirected to the main page

class NewSale(BaseHandler, blobstore_handlers.BlobstoreUploadHandler):
	"""
	View for creating a new sale.
	"""

	GOOGLE_KEY = "AIzaSyBxqJnwKixPXr4nIq2pgCLktrhooGe10K4"

	GOOGLE_MAPS_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?query={0}&key={1}"

	GOOGLE_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json?reference={0}&key={1}"

	def register_fail(self, reason):
		json_response = {}
		json_response[u'status'] = False
		json_response[u'reason'] = reason
		self.response.write(json.dumps(json_response, indent=4))
		return

	def get(self):
		# Display create new sale form
		self.response.headers['Content-Type'] = 'text/html; charset=UTF-8'
		upload_url = blobstore.create_upload_url("/new_sale")
		context = {}
		context["upload_url"] = upload_url
		q = SXP_Product.query()
		products = q.fetch()
		q2 = SXP_Merchant.query()
		merchants = q2.fetch()
		context['products'] = products
		context['merchants'] = merchants
		template = JINJA_ENVIRONMENT.get_template('new_sale.html')
		self.response.write(template.render(context))

	def post(self):
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		try:
			person_id = self.request.cookies["id"]
		except KeyError:
			redirect('/')
			return
		logging.info(u'Parameters: '+self.request.get('merchant_name')+u' '+self.request.get('product_name')+u' '+self.request.get('end_date')+u' '+self.request.get('discount')+u' '+self.request.get('product_price'))
		try:
			logging.info('User cookie: '+person_id)
			logging.info('Merchant name: '+self.pDecode(self.request.get('merchant_name')))
			logging.info('Merchant location: '+self.pDecode(self.request.get('merchant_location')))
			logging.info('Product Name: '+self.pDecode(self.request.get('product_name')))
			logging.info('Product Price: '+self.pDecode(self.request.get('product_price')))
			logging.info('End Date: '+self.pDecode(self.request.get('end_date')))
			logging.info('Discount: '+self.pDecode(self.request.get('discount')))
			logging.info('Hashtags: '+self.pDecode(self.request.get('hashtags')))
		except Exception, e:
			logging.error(str(e))
		q = SXP_Person.query(SXP_Person.cookie == person_id)
		people = q.fetch()
		if len(people) <= 0:
			# Cookie invalid
			self.register_fail("Invalid user cookie")
			return
		person = people[0] # person
		# Register a new sale
		merchant = None
		if self.request.POST.get('merchant') and not (self.request.POST.get('merchant') == "other_merchant"):
			i_id = 0
			try:
				i_id = int(self.request.POST.get('merchant'))
			except:
				self.register_fail("Selected merchant was not found")
				return
			merchant = SXP_Merchant.get_by_id(i_id)
			if merchant == None:
				self.register_fail("Selected merchant was not found")
				return
		else:
			# Create a new merchant
			merchant = SXP_Merchant()
			merchant.name = self.pDecode(self.request.get('merchant_name'))
			merchant.location = self.pDecode(self.request.get('merchant_location'))
			loc_info = {}
			try:
				loc = str(merchant.location)
				loc = loc.replace(" ", "+")
				name = str(merchant.name)
				name = name.replace(" ", "+")
				arg = NewSale.GOOGLE_MAPS_URL.format(name+",+"+loc, NewSale.GOOGLE_KEY)
				url = urllib2.urlopen(arg)
				loc_info = json.loads(url.read())
			except urllib2.HTTPError:
				# Failed to get google geocoding
				# We don't know if this location is valid. Better to trust the user?
				# For now, cancel registration
				self.register_fail("It was not possible to confirm location existance")
				return
			if loc_info['status'] != "OK":
				# Google says location is invalid
				self.register_fail("Invalid location")
				return
			location = loc_info['results'][0]
			lat = location['geometry']['location']['lat']
			lon = location['geometry']['location']['lng']
			geoPos = ndb.GeoPt(lat, lon)
			q = SXP_Merchant.query(SXP_Merchant.geoPos == geoPos)
			merchants = q.fetch()
			if len(merchants) > 0:
				merchant = merchants[0]
			else:
				merchant.geoPos = geoPos
				reference = str(location['reference'])
				loc_types = str(location['types'])
				try:
					url = NewSale.GOOGLE_DETAILS_URL.format(reference, NewSale.GOOGLE_KEY)
					conn = urllib2.urlopen(url)
					details = json.loads(conn.read())
				except:
					self.register_fail("It was not possible to confirm location existance")
					return
				if(details['status'] != "OK"):
					self.register_fail("Invalid location")
					return
				loc_comp = details['result']['address_components']
				merchant.location = details['result']['formatted_address']
				merchant.name = details['result']['name']
				for add_comp in loc_comp:
					if ("locality" in add_comp['types']) or ("postal_town" in add_comp['types']):
						merchant.city = add_comp['long_name']
					if "country" in add_comp['types']:
						merchant.country = add_comp['long_name']
					if "postal_code" in add_comp['types']:
						merchant.postal_code = add_comp['long_name']
					if "administrative_area_level_2" in add_comp['types']:
						merchant.county = add_comp['long_name']
					if "administrative_area_level_1" in add_comp['types']:
						merchant.state = add_comp['long_name']
				if "international_phone_number" in details['result'].keys():
					merchant.phone_number = details['result']['international_phone_number']
		product = None
		if self.request.get('product') and not (self.request.POST.get('product') == "other_product"):
			p_id = 0
			try:
				p_id = int(self.pDecode(self.request.get('product')))
			except:
				self.register_fail("Selected product was not found")
				return
			product = SXP_Product.get_by_id(p_id)
			if product == None:
				self.register_fail("Selected product was not found")
				return
		else:
			q = SXP_Product.query(SXP_Product.name == self.pDecode(self.request.get('product_name')))
			products = q.fetch()
			if len(products) <= 0:
				product = SXP_Product()
				product.name = self.pDecode(self.request.get('product_name'))
				file_upload = self.get_uploads()
				if len(file_upload) > 0:
					img = file_upload[0]
					product.image = img.key()
			else:
				product = products[0]
				file_upload = self.get_uploads()
				if len(file_upload) > 0:
					img = file_upload[0]
					product.image = img.key()
		if not self.request.get('discount'):
			self.register_fail("Discount must be provided")
			return
		sale = SXP_Sale()
		if self.request.get('end_date'):
			date = None
			try:
				date = datetime.datetime.strptime(self.pDecode(self.request.get('end_date')) + " 23:59:59", "%d/%m/%Y %H:%M:%S")
			except:
				self.register_fail("Invalid date provided")
				return
			if date < datetime.datetime.today():
				self.register_fail("Invalid date provided")
				return
			sale.end_date = date
		sale.discount = self.pDecode(self.request.get('discount'))
		if self.request.get('product_price'):
			try:
				sale.price = float(self.pDecode(self.request.get('product_price')))
			except:
				self.register_fail("Invalid original price provided")
				return
		sale.owner = person.key
		merchant.put()
		product.put()
		sale.product = product.key
		sale.merchant = merchant.key
		sale.put()

		try:	
			# Get hashtags
			if self.request.get('hashtags'):
				hashtags = self.pDecode(self.request.get('hashtags'))
				tagsList = re.findall("#(\w+)+", hashtags)
				for tag in tagsList:
					saleTag = SXP_SalesHashTag()
					saleTag.sale = sale.key
					hashtag = SXP_HashTag()
					hashtag.tag = unicode(tag)
					hashtag.put()
					saleTag.hashtag = hashtag.key
					saleTag.put()
		except Exception, e:
			logging.error(e)

		json_response = {}
		json_response[u'status'] = True
		self.response.write(json.dumps(json_response, indent=4))
		return

class ProductPictures(BaseHandler, blobstore_handlers.BlobstoreDownloadHandler):
	"""
	Class to handle request to view product pictures
	"""
	def get(self, resource):
		if resource == None:
			self.response.headers['Content-Type'] = 'application/json'
			json_response = {}
			json_response[u'status'] = False
			self.response.write(json.dumps(json_response, indent=4))
			return
		else:
			resource = str(urllib.unquote(resource))
			blob_info = blobstore.BlobInfo.get(resource)
			self.send_blob(blob_info)

class Product(BaseHandler):
	"""
	Class to handle requests to view product information
	"""
	def prodNotFound(self):
		"""
		Returns an error in case it was not possible to find the product
		"""
		json_response = {}
		json_response[u'status'] = False
		json_response[u'reason'] = "Failed to provide a valid product ID"
		self.response.write(json.dumps(json_response, indent=4))
		return

	def get(self, prod_id):
		"""
		Returns product information or Error in case it was not found
		"""
		self.response.headers['Content-Type'] = 'application/json'
		# Try to find product
		if prod_id == None:
			self.prodNotFound() # Failed
			return
		try:
			prod_id = int(prod_id)
		except:
			self.prodNotFound() # Failed
			return
		product = SXP_Product.get_by_id(prod_id)
		if product == None:
			self.prodNotFound() # Failed
			return
		# Product was found
		json_response = {u'status': True}
		img_url = self.request.host_url + "/prod_pic/" + str(product.image)
		json_response[u'product'] = {u'name': unicode(product.name), u'image_url': unicode(img_url)}
		self.response.write(json.dumps(json_response, indent=4))
		return

class Sale(BaseHandler):
	"""
	Class to handle requests to view Sale's information
	"""
	def saleNotFound(self):
		"""
		Returns an error in case it was not possible to find the Sale
		"""
		json_response = {}
		json_response[u'status'] = False
		json_response[u'reason'] = "Failed to provide a valid Sale ID"
		self.response.write(json.dumps(json_response, indent=4))
		return

	def get(self, sale_id):
		"""
		Returns sale information with product and merchant information as well.

		Returns an error in case the Sale was not found.
		"""
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		# Try to find Sale
		if sale_id == None:
			self.saleNotFound() # Failed
			return
		try:
			sale_id = int(sale_id)
		except:
			self.saleNotFound() # Failed
			return
		sale = SXP_Sale.get_by_id(sale_id)
		if sale == None:
			self.saleNotFound() # Failed
			return
		# Sale was found. Build JSONObject
		json_response = {u'status': True}
		base_url = self.request.host_url
		json_response[u'sale'] = sale.toJson(base_url)
		self.response.write(json.dumps(json_response, indent=4))
		return

class RelevantSales(BaseHandler):
	"""
	Handler to display relevant sales according to the user's location
	"""

	SLICE_SIZE = 15

	def distance(self, lat1, lon1, lat2, lon2):
		theta = lon1 - lon2
		dist = math.sin(self.deg2rad(lat1)) * math.sin(self.deg2rad(lat2)) + math.cos(self.deg2rad(lat1)) * math.cos(self.deg2rad(lat2)) * math.cos(self.deg2rad(theta))
		dist = math.acos(dist)
		dist = self.rad2deg(dist)
		dist = dist * 60 * 1.1515
		dist = dist * 1.609344
		return dist

	def deg2rad(self, deg):
		return (deg * math.pi / 180.0)

	def rad2deg(self, rad):
		return (rad * 180.0 / math.pi)

	def get(self):
		"""
		"""
		try:
			self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
			json_response = {u'status': False}
			if not self.request.get("loc") and (not self.request.get("lat") or not self.request.get("lon")):
				json_response[u'reason'] = "No location provided"
				self.response.write(json.dumps(json_response, indent=4))
				return
			lon = 0.0
			lat = 0.0
			try:
				if self.request.get("loc"):
					lat = float(self.request.get("loc").split(";")[0])
					lon = float(self.request.get("loc").split(";")[1])
				else:
					lat = float(self.request.get("lat"))
					lon = float(self.request.get("lon"))
			except:
				json_response[u'reason'] = "Invalid location provided"
				self.response.write(json.dumps(json_response, indent=4))
				return
			q = SXP_Sale.query(SXP_Sale.valid == True)
			sales = q.fetch()
			result = []
			for sale in sales:
				merchant = SXP_Merchant.get_by_id(sale.merchant.id())
				m_lat = merchant.geoPos.lat
				m_lon = merchant.geoPos.lon
				result.append((self.distance(lat, lon, m_lat, m_lon), sale))
			result.sort(key=lambda tup: tup[0])
			try:
				offset = int(self.request.get('offset')) if self.request.get('offset') else 0
			except:
				json_response[u'reason'] = "Invalid offset"
				self.response.write(json.dumps(json_response, indent=4))
				return
			try:
				if(len(result) < RelevantSales.SLICE_SIZE+(offset*RelevantSales.SLICE_SIZE)):
					result = result[(offset*RelevantSales.SLICE_SIZE):len(result)]
				else:
					result = result[(offset*RelevantSales.SLICE_SIZE):RelevantSales.SLICE_SIZE+(offset*RelevantSales.SLICE_SIZE)]
			except:
				result = []
			json_response[u'status'] = True
			base_url = self.request.host_url
			json_response[u'sales'] = []
			for r in result:
				json_response[u'sales'] += [r[1].toJson(base_url, r[0])]
			self.response.write(json.dumps(json_response, indent=4))
			return
		except Exception, e:
			logging.error(e)
			logging.error(traceback.format_exc())

class NewSalesTrigger(BaseHandler):
	"""
	Create a new sales trigger
	"""
	def get(self):
		# Display create new sale form
		self.response.headers['Content-Type'] = 'text/html; charset=UTF-8'
		context = {}
		q = SXP_Product.query()
		products = q.fetch()
		context['products'] = products
		template = JINJA_ENVIRONMENT.get_template('new_sales_trigger.html')
		self.response.write(template.render(context))

	def post(self):
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		json_response = {}
		json_response[u'status'] = False
		try:
			person_id = self.request.cookies['id']
		except KeyError:
			self.redirect('/')
			return
		q = SXP_Person.query(SXP_Person.cookie == person_id)
		people = q.fetch()
		if len(people) <= 0:
			# If the cookie does not belong to any user
			self.clear_cookie("id")
			self.redirect('/login')
			return
		person = people[0]
		product = None
		if self.request.get('product') and not (self.request.POST.get('product') == "other_product"):
			p_id = 0
			try:
				p_id = int(self.request.get('product'))
			except:
				json_response[u'reason'] = "Invalid product"
				self.response.write(json.dumps(json_response, indent=4))
				return
			product = SXP_Product.get_by_id(p_id)
			if not product:
				json_response[u'reason'] = "Invalid product"
				self.response.write(json.dumps(json_response, indent=4))
				return
		else:
			p_name = self.request.get('product_name')
			product = SXP_Product(name=p_name)
		max_dist = 0.0
		try:
			max_dist = float(self.request.get('max_dist'))
		except:
			json_response[u'reason'] = "Invalid maximum distance"
			self.response.write(json.dumps(json_response, indent=4))
			return
		if max_dist < 0.0:
			json_response[u'reason'] = "Invalid maximum distance"
			self.response.write(json.dumps(json_response, indent=4))
			return
		product.put()
		saleTrigger = SXP_SalesTrigger()
		saleTrigger.product = product.key
		saleTrigger.max_dist = max_dist
		saleTrigger.owner = person.key
		saleTrigger.put()
		json_response[u'status'] = True
		self.response.write(json.dumps(json_response, indent=4))
		return


class SalesNotifications(BaseHandler):
	"""
	"""

	def get(self):
		"""
		"""
		base_url = self.request.host_url
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		json_response = {}
		json_response[u'status'] = False
		# Check if input is correct
		lat = 0.0
		lon = 0.0
		try:
			lat = float(self.request.get('lat'))
			lon = float(self.request.get('lon'))
		except:
			json_response[u'reason'] = "Invalid GeoPosition provided"
			return
		# Check if user is logged in
		try:
			person_id = self.request.cookies['id']
		except KeyError:
			# User is not logged in
			self.redirect('/')
		q = SXP_Person.query(SXP_Person.cookie == person_id)
		people = q.fetch()
		if len(people) <= 0:
			# If the cookie does not belong to any user
			# User is not logged in
			self.clear_cookie("id")
			self.redirect('/login')
			return
		person = people[0]
		# User is logged in
		q = SXP_Notification.query(SXP_Notification.destination == person).order(-SXP_Notification.date)
		notifs = q.fetch()
		q = SXP_SalesTrigger.query(SXP_SalesTrigger.owner == person.key)
		triggers = q.fetch()
		notifications = []
		for trig in triggers:
			product = SXP_Product.get_by_id(trig.product.id())
			q = SXP_Sale.query(SXP_Sale.valid == True, SXP_Sale.product == product.key)
			sales = q.fetch()
			result = []
			for sale in sales:
				merchant = SXP_Merchant.get_by_id(sale.merchant.id())
				m_lat = merchant.geoPos.lat
				m_lon = merchant.geoPos.lon
				leg1 = (lat - m_lat)*(lat - m_lat)
				leg2 = (lon - m_lon)*(lon - m_lon)
				distance = math.sqrt(leg1 + leg2)
				if distance <= trig.max_dist:
					result.append(sale)
					notification = SXP_Notification()
					notification.destination = person
					notification.message = u'Product \"'+product.name+u'\" is on sale at '+merchant.name
					notification.date = datetime.datetime.now()
					notification.not_type = "Sales"
					notification.sale_id = sale.key
					notification.put()
					notifications.append(notification)
		notifications += notifs
		json_response[u'status'] = True
		json_response[u'notifications'] = []
		for notification in notifications:
			json_response[u'notifications'].append(notification.toJson(base_url))
		self.response.write(json.dumps(json_response, indent=4))
		return

class ListMerchants(BaseHandler):
	"""
	Returns a list of all merchants currently registered.
	"""

	def get(self):
		base_url = self.request.host_url
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		json_response = {}
		first_letter = self.request.get('first_letter')
		if first_letter:
			try:
				q = SXP_Merchant.query()
				mercs = q.fetch()
				merchants = []
				for merc in mercs:
					if str(merc.name).lower().startswith(first_letter.lower()):
						merchants.append(merc)
			except Exception, e:
				logging.error(e)
				return
		else:
			q = SXP_Merchant.query()
			merchants = q.fetch()
		try:
			json_response[u'status'] = True
			json_response[u'merchants'] = []
			for merchant in merchants:
				json_response[u'merchants'].append(merchant.toJson(base_url))
			self.response.write(json.dumps(json_response, indent=4))
			return
		except Exception, e:
			logging.error(e)
			return

class HashTagSearch(BaseHandler):
	"""
	"""

	# Number of sales that will be displayed on each query
	NUMBER_OF_DISPLAY = 10

	def get(self):
		base_url = self.request.host_url
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		json_response = {}
		try:
			if self.request.get('tag'):
				searchTag = self.request.get('tag')
				q = SXP_HashTag.query()
				all_hashtags = q.fetch()
				hashtags = []

				# Calculate distances
				for hashtag in all_hashtags:
					distance = levenshtein(hashtag.tag.encode("UTF-8").lower(), searchTag.encode("UTF-8").lower())
					hashtags.append((distance, hashtag))
					#delete this
					if distance == 4:
						logging.info("tag = "+hashtag.tag.encode("UTF-8"))
				# hashtags.sort(key=lambda tup: tup[0])
				# if len(hashtags) > HashTagSearch.NUMBER_OF_DISPLAY:
				# 	hashtags = hashtags[0:HashTagSearch.NUMBER_OF_DISPLAY-1]
					
				#Getting the SXP_Sale objects from the hashtag search
				json_response[u'status'] = True
				sales = {}
				for distance, hashtag in hashtags:
					q = SXP_SalesHashTag.query(SXP_SalesHashTag.hashtag == hashtag.key)
					saleHashTags = q.fetch()
					for saleHashTag in saleHashTags:
						sale = SXP_Sale.get_by_id(saleHashTag.sale.id())
						if sale.key.id() in sales.keys():
							old_distance = sales[saleHashTag.sale.id()][0]
							if old_distance > distance:
								sales[saleHashTag.sale.id()] = (distance, sale)
						else:
							sales[saleHashTag.sale.id()] = (distance, sale)

				#Getting the SXP_Sale objects from the Name search
				q = SXP_Sale.query()
				all_sales = q.fetch()
				for sale in all_sales:
					product = SXP_Product.get_by_id(sale.product.id())
					distance = levenshtein(product.name.encode("UTF-8").lower(), searchTag.encode("UTF-8").lower())
					if sale.key.id() in sales.keys():
						old_distance = sales[sale.key.id()][0]
						if old_distance > distance:
							sales[sale.key.id()] = (distance, sale)
					else:
						sales[sale.key.id()] = (distance, sale)
				# Build json response
				json_response[u'results'] = []
				all_sales = sales.values()
				all_sales.sort(key=lambda tup: tup[0])
				if len(all_sales) > HashTagSearch.NUMBER_OF_DISPLAY:
					all_sales = all_sales[0:HashTagSearch.NUMBER_OF_DISPLAY-1]
				for distance, sale in all_sales:
					logging.info("Distance: "+str(distance)+"\n"+"Sale: "+str(sale)+"\n\n")
					json_response[u'results'].append(sale.toJson(base_url))
				self.response.write(json.dumps(json_response, indent=4))
				return
			else:
				json_response[u'status'] = False
				json_response[u'reason'] = u'Search tag not provided'
				self.response.write(json.dumps(json_response, indent=4))
				return
		except Exception, e:
			logging.error(e)
			logging.error(traceback.format_exc())


class LikeSale(BaseHandler):
	"""
	"""
	def get(self):
		base_url = self.request.host_url
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		json_response = {}
		# Get logged in user
		try:
			person_id = self.request.cookies['id']
		except KeyError:
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Not logged in")
			self.response.write(json.dumps(json_response, indent=4))
			return
		q = SXP_Person.query(SXP_Person.cookie == person_id)
		people = q.fetch()
		if len(people) <= 0:
			# If the cookie does not belong to any user
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Not logged in")
			self.response.write(json.dumps(json_response, indent=4))
			return
		person = people[0]
		# got the user
		if not self.request.get('sale_id'):
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Sale id was not provided")
			self.response.write(json.dumps(json_response, indent=4))
			return
		sale_id = 0
		try:
			sale_id = int(self.request.get('sale_id'))
		except:
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Invalid sale ID provided")
			self.response.write(json.dumps(json_response, indent=4))
			return
		sale = SXP_Sale.get_by_id(sale_id)
		if not sale:
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Invalid sale ID provided")
			self.response.write(json.dumps(json_response, indent=4))
			return
		# See if user has liked this sale before
		try:
			q = SXP_SalesLike.query(SXP_SalesLike.person == person.key, SXP_SalesLike.sale == sale.key)
			likes = q.fetch()
			if len(likes) > 0:
				json_response[u'status'] = False
				json_response[u'reason'] = unicode("You already liked this sale!")
				self.response.write(json.dumps(json_response, indent=4))
				return
		except Exception, e:
			logging.error(e)
		try:
			num_likes = sale.num_like
			sale.num_like = num_likes + 1
		except:
			sale.num_like = 1
		try:
			logging.info("Like Object creation started")
			like = SXP_SalesLike()
			like.person = person.key
			like.sale = sale.key
			logging.info("Like Object created!")
		except Exception, e:
			logging.error(e)
		sale.put()
		like.put()
		logging.info("Like Object pushed to database")
		json_response[u'status'] = True
		self.response.write(json.dumps(json_response, indent=4))
		return

class ReportSale(BaseHandler):
	"""
	"""
	def get(self):
		base_url = self.request.host_url
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
		json_response = {}
		# Get logged in user
		try:
			person_id = self.request.cookies['id']
		except KeyError:
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Not logged in")
			self.response.write(json.dumps(json_response, indent=4))
			return
		q = SXP_Person.query(SXP_Person.cookie == person_id)
		people = q.fetch()
		if len(people) <= 0:
			# If the cookie does not belong to any user
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Not logged in")
			self.response.write(json.dumps(json_response, indent=4))
			return
		person = people[0]
		# got the user
		if not self.request.get('sale_id'):
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Sale id was not provided")
			self.response.write(json.dumps(json_response, indent=4))
			return
		sale_id = 0
		try:
			sale_id = int(self.request.get('sale_id'))
		except:
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Invalid sale ID provided")
			self.response.write(json.dumps(json_response, indent=4))
			return
		sale = SXP_Sale.get_by_id(sale_id)
		if not sale:
			json_response[u'status'] = False
			json_response[u'reason'] = unicode("Invalid sale ID provided")
			self.response.write(json.dumps(json_response, indent=4))
			return
		# See if the user has already reported this sale
		try:
			q = SXP_SaleReport.query(SXP_SaleReport.sale == sale.key, SXP_SaleReport.reporter == person.key)
			reports = q.fetch()
			if len(reports) > 0:
				json_response[u'status'] = False
				json_response[u'reason'] = unicode("You already reported this sale!")
				self.response.write(json.dumps(json_response, indent=4))
				return
		except Exception, e:
			logging.error(e)
		# Finally, everything is tested
		try:
			num_report = sale.num_report
			sale.num_report = num_report + 1
		except:
			sale.num_report = 1
		try:
			report = SXP_SaleReport()
			report.reporter = person.key
			report.sale = sale.key
		except Exception, e:
			logging.error(e)
		sale.put()
		report.put()
		json_response[u'status'] = True
		json_response[u'report'] = report.toJson(base_url)
		self.response.write(json.dumps(json_response, indent=4))
		return

class UserProfile(BaseHandler):
	"""
	"""
	def get(self):
		try:
			self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
			json_response = {}
			json_response[u'status'] = False
			try:
				person_id = self.request.cookies['id']
			except KeyError:
				json_response[u'reason'] = unicode("Not logged in")
				self.response.write(json.dumps(json_response, indent=4))
				return
			person = SXP_Person.query(SXP_Person.cookie == person_id).fetch()[0]
			json_response[u'profile'] = person.toJson()
			json_response[u'status'] = True
			self.response.write(json.dumps(json_response, indent=4))
			return
		except Exception, e:
			logging.error(traceback.format_exc())

class MainPage(BaseHandler):
    def get(self):
    	self.response.headers['Content-Type'] = 'text/html'
    	context = {}
    	try:
    		person_id = self.request.cookies["id"]
    		q = SXP_Person.query(SXP_Person.cookie == person_id)
    		person = q.fetch()[0]
    		context["person"] = person
    	except KeyError:
    		pass
    	except IndexError:
    		pass
    	template = JINJA_ENVIRONMENT.get_template('base_template.html')
    	self.response.write(template.render(context))
