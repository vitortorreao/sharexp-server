import os
import webapp2

from sxp_views import *

config = {}
config['webapp2_extras.sessions'] = {
    'secret_key': '6@9d$8((u#lkj(fi+gv-6-=coqc%7r*a^=^&hd&iufqj19(9pz',
}

application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/login', Login),
    ('/register', Register),
    ('/logout', Logout),
    ('/new_sale', NewSale),
    ('/product/([\d]+)?', Product),
    ('/prod_pic/([^/]+)?', ProductPictures),
    ('/sale/([\d]+)?', Sale),
    ('/sales', RelevantSales),
    ('/new_sales_trigger', NewSalesTrigger),
    ('/sales_notifications', SalesNotifications),
    ('/merchants', ListMerchants),
    ('/report', ReportSale),
    ('/hashtag_search', HashTagSearch),
    ('/profile', UserProfile),
    ('/like', LikeSale)
], config=config, debug=True)
